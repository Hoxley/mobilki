package com.example.boardgame.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boardgame.R;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class QRScanActivity extends AppCompatActivity {

    SurfaceView surfaceView;
    CameraSource cameraSource;
    TextView qrTextView;
    BarcodeDetector barcodeDetector;
    Button rentGameButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scan);
        surfaceView = findViewById(R.id.camerapreview);
        qrTextView = findViewById(R.id.textViewQR);
        qrTextView.setVisibility(View.VISIBLE);
        rentGameButton = findViewById(R.id.buttonRentGame);

        rentGameButton.setOnClickListener(view -> {
            if (!qrTextView.getText().equals("Proszę zeskanować kod QR")) {
                Intent intent = new Intent(QRScanActivity.this, PayPalActivity.class);
                intent.putExtra("id", qrTextView.getText());
                startActivity(intent);
            }
        });

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE).build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                try {
                    cameraSource.start(surfaceHolder);
                } catch (IOException e) {
                    Log.e("apk", e.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> qrCodes = detections.getDetectedItems();

                if (qrCodes.size() != 0) {
                    qrTextView.post(() -> {
                        Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrator.vibrate(1000);
                        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                        firebaseFirestore.collection("BoardGames").document(qrCodes.valueAt(0).displayValue).get().addOnSuccessListener(runnable -> {
                            boolean ordered = Boolean.valueOf(Objects.toString(runnable.getData().getOrDefault("ordered", "")));
                            if (ordered) {
                                Toast.makeText(getApplicationContext(), "Ups... Ta planszówka jest już wypożyczona!", Toast.LENGTH_SHORT).show();
                            } else {
                                qrTextView.setText(qrCodes.valueAt(0).displayValue);
                                qrTextView.setVisibility(View.INVISIBLE);
                            }
                        });
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        qrTextView.setText("Proszę zeskanować kod QR");
        qrTextView.setVisibility(View.VISIBLE);
    }
}
