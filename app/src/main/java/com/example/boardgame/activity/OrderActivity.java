package com.example.boardgame.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import com.example.boardgame.R;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class OrderActivity extends AppCompatActivity {

    private EditText name;
    private EditText surname;
    private EditText email;
    private EditText address;
    private Set<ValidatableEditText> validatableEditTextList = new HashSet<>();
    private Button orderButton;

    private String boardGameName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        boardGameName = getIntent().getExtras().getString("gameName");

        name = findViewById(R.id.name);
        validatableEditTextList.add(new ValidatableEditText(name, new EditTextLengthValidator()));

        surname = findViewById(R.id.surname);
        validatableEditTextList.add(new ValidatableEditText(surname, new EditTextLengthValidator()));

        email = findViewById(R.id.email);
        validatableEditTextList.add(new ValidatableEditText(email, new EmailEditTextValidator()));

        address = findViewById(R.id.address);
        validatableEditTextList.add(new ValidatableEditText(address, new EditTextLengthValidator()));

        orderButton = findViewById(R.id.order);

        validatableEditTextList.forEach(editText -> editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //implementation not needed
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editText.touch();
                validateForm();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //implementation not needed
            }
        }));

        orderButton.setOnClickListener(v -> {
            Intent intent = new Intent(OrderActivity.this, PayPalActivity.class);
            Map<String, String> order = new HashMap<>();
            order.put("name", name.getText().toString());
            order.put("surname", surname.getText().toString());
            order.put("email", email.getText().toString());
            order.put("address", address.getText().toString());
            order.put("boardGameName", boardGameName);
            intent.putExtra("order", (Serializable) order);
            intent.putExtra("id", getIntent().getStringExtra("id"));
            startActivity(intent);
        });
    }

    private void validateForm() {
        boolean formValid = validatableEditTextList.stream().allMatch(ValidatableEditText::test);
        orderButton.setEnabled(formValid);
    }

    private class ValidatableEditText {
        private EditText editText;
        private boolean touched = false;
        private Predicate<EditText> validateFunction;

        ValidatableEditText(EditText editText, Predicate<EditText> validateFunction) {
            this.editText = editText;
            this.validateFunction = validateFunction;
        }

        void touch() {
            this.touched = true;
        }

        void addTextChangedListener(TextWatcher textWatcher){
            editText.addTextChangedListener(textWatcher);
        }

        boolean test(){
            if(touched){
                return validateFunction.test(editText);
            }
            return false;
        }
    }

    private class EditTextLengthValidator implements Predicate<EditText> {

        private static final String EMPTY_FIELD_ERROR_MESSAGE = "Fill this field";

        @Override
        public boolean test(EditText editText) {
            boolean isValid = editText.getText().toString().trim().length() > 0;
            if(!isValid) {
                editText.setError(EMPTY_FIELD_ERROR_MESSAGE);
            }
            return isValid;
        }
    }

    private class EmailEditTextValidator extends EditTextLengthValidator {

        private static final String INCORRECT_EMAIL_MESSAGE = "Fix your email address";

        private final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        @Override
        public boolean test(EditText editText) {
            boolean isValid = super.test(editText) &&
                    VALID_EMAIL_ADDRESS_REGEX.matcher(editText.getText().toString().trim()).find();
            if(!isValid) {
                editText.setError(INCORRECT_EMAIL_MESSAGE);
            }
            return isValid;
        }
    }
}
