package com.example.boardgame.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.boardgame.MainActivity;
import com.example.boardgame.R;
import com.example.boardgame.adapter.BoardGameAdapter;
import com.example.boardgame.model.BoardGame;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import io.opencensus.resource.Resource;

public class GamesListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_games_list);
        Optional.ofNullable(getIntent().getExtras()).ifPresent(bundle -> {
            if (bundle.getBoolean("orderStatus")) {
                Toast.makeText(getApplicationContext(), "Order successful!", Toast.LENGTH_SHORT).show();
            }
        });

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference games = db.collection("BoardGames");
        ListView listView = findViewById(R.id.gamesListView);
        List<BoardGame> gamesList = new ArrayList<>();
        BoardGameAdapter adapter = new BoardGameAdapter(this, R.layout.adapter_view_layout_games, gamesList);

        games.get().addOnSuccessListener(documentSnapshots -> {
            documentSnapshots.iterator().forEachRemaining(record -> {
                BoardGame boardGame = new BoardGame(
                        (record.getId()),
                        Objects.toString(record.getData().get("name")),
                        Objects.toString(record.getData().get("description")),
                        Objects.toString(record.getData().get("uri")),
                        Boolean.valueOf(Objects.toString(record.getData().get("ordered"))));
                if (!boardGame.isOrdered()) {
                    adapter.add(boardGame);
                }
            });
        });

        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(GamesListActivity.this, OrderActivity.class);
            gamesList.stream().skip(position).findFirst().ifPresent(game -> {
                intent.putExtra("gameName", game.getName());
                intent.putExtra("id", game.getId());
            });
            startActivity(intent);
        });
    }
}
