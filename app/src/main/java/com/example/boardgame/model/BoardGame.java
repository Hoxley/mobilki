package com.example.boardgame.model;


import android.net.Uri;

public class BoardGame {

    private String id;
    private String name;
    private String description;
    private String uri;
    private boolean ordered;

    public BoardGame(String id, String name, String description, String uri, boolean ordered) {
        this.name = name;
        this.description = description;
        this.uri = uri;
        this.id = id;
        this.ordered = ordered;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }
}
