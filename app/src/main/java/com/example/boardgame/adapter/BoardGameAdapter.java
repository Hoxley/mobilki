package com.example.boardgame.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.boardgame.R;
import com.example.boardgame.model.BoardGame;

import java.io.InputStream;
import java.util.List;

import androidx.annotation.NonNull;

public class BoardGameAdapter extends ArrayAdapter<BoardGame> {

    private LayoutInflater layoutInflater;
    private List<BoardGame> gamesList;
    private int mViewResourceId;

    public BoardGameAdapter(@NonNull Context context, int textViewResourceId, List<BoardGame> gamesList) {
        super(context, textViewResourceId, gamesList);
        this.gamesList = gamesList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

    @Override
    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parents) {
        convertView = layoutInflater.inflate(mViewResourceId, null);

        BoardGame boardGame = gamesList.get(position);

        if (boardGame != null) {
            TextView gameName = convertView.findViewById(R.id.textViewGameName);
            TextView gameDesc = convertView.findViewById(R.id.textViewGameDesc);
            ImageView gameImage = convertView.findViewById(R.id.imageViewGameImage);

            gameName.setText(boardGame.getName());
            gameDesc.setText(boardGame.getDescription());
            new DownloadImageTask(gameImage)
                    .execute(boardGame.getUri());
        }
        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
