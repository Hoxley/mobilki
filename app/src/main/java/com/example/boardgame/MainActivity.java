package com.example.boardgame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.boardgame.activity.GamesListActivity;
import com.example.boardgame.activity.PayPalActivity;
import com.example.boardgame.activity.QRScanActivity;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonQR = findViewById(R.id.buttonQR);
        Button buttonGameList = findViewById(R.id.buttonGameList);

        buttonQR.setOnClickListener(view -> {
            Intent intent = new Intent(
                    MainActivity.this, QRScanActivity.class);
            startActivity(intent);
        });

        buttonGameList.setOnClickListener(view -> {
                    Intent intent = new Intent(
                            MainActivity.this, GamesListActivity.class);
                    startActivity(intent);
                }
        );
    }
}
